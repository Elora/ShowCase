package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.model.Tag;

import java.util.Optional;
import java.util.Set;

public interface TagService extends DefaultService<Tag> {
    public Set<Tag> findByName(String name);
}
