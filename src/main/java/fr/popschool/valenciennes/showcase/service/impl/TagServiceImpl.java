package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.model.Tag;
import fr.popschool.valenciennes.showcase.model.User;
import fr.popschool.valenciennes.showcase.repository.ProjectRepository;
import fr.popschool.valenciennes.showcase.repository.TagRepository;
import fr.popschool.valenciennes.showcase.repository.UserRepository;
import fr.popschool.valenciennes.showcase.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TagServiceImpl implements TagService {
    @Autowired
    TagRepository tagRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Override
    public Set<Tag> findByName(String name) {
        return tagRepository.findByName(name);
    }

    @Override
    public Optional<Tag> findById(Long id) {
        return tagRepository.findById(id);
    }

    @Override
    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    @Override
    public Tag save(Tag object) {
        Set<User> users = new HashSet();
        for (User us :
                object.getUsers()) {
            users.add(userRepository.findById(us.getId()).orElseThrow(() -> new ResourceNotFoundException("User", "id", us.getId())));
        }
        object.setUsers(new HashSet<>());
        object.addUsers(users);

        Set<Project> projects = new HashSet<>();
        for (Project p : object.getProjects()){
            if(p.getId() == null){
                projects.add(projectRepository.save(p));
            }else{
                projects.add(projectRepository.findById(p.getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", p.getId())));
            }
        }
        object.setProjects(new HashSet<>());
        object.addProjects(projects);

        return tagRepository.save(object);
    }

    @Override
    public void delete(Tag object) {
        tagRepository.delete(object);
    }


}
