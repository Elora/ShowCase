package fr.popschool.valenciennes.showcase.service;

import org.springframework.core.convert.support.DefaultConversionService;
import fr.popschool.valenciennes.showcase.model.Project;

import java.util.List;

public interface ProjectService extends DefaultService<Project>{

    List<Project> findByCreator(String creator);
    List<Project> findByName(String name);

}
