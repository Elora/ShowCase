package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Link;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.repository.LinkRepository;
import fr.popschool.valenciennes.showcase.repository.ProjectRepository;
import fr.popschool.valenciennes.showcase.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class LinkServiceImpl implements LinkService {
    @Autowired
    LinkRepository linkRepository;
    @Autowired
    ProjectRepository projectRepository;


    @Override
    public Optional<Link> findByLink(String link) {
        return linkRepository.findByLink(link);
    }

    @Override
    public Optional<Link> findById(Long id) {
        return linkRepository.findById(id);
    }

    @Override
    public List<Link> findAll() {
        return linkRepository.findAll();
    }

    @Override
    public Link save(Link object) {
        Project project= new Project();
        if(object.getProjects().getId() == null){
            object.setProjects(projectRepository.save(project));
        }else{
            project=projectRepository.findById(object.getProjects().getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", object.getProjects().getId()));
        }

        object.setProjects(new Project());
        object.addProject(project);
        return linkRepository.save(object);
    }

    @Override
    public void delete(Link object) {
        linkRepository.delete(object);
    }
}
