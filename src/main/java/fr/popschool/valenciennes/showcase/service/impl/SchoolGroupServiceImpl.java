package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Course;
import fr.popschool.valenciennes.showcase.model.Factory;
import fr.popschool.valenciennes.showcase.model.SchoolGroup;
import fr.popschool.valenciennes.showcase.repository.CourseRepository;
import fr.popschool.valenciennes.showcase.repository.FactoryRepository;
import fr.popschool.valenciennes.showcase.repository.SchoolGroupRepository;
import fr.popschool.valenciennes.showcase.service.SchoolGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SchoolGroupServiceImpl implements SchoolGroupService {

    @Autowired
    SchoolGroupRepository schoolGroupRepository;
    @Autowired
    FactoryRepository factoryRepository;
    @Autowired
    CourseRepository courseRepository;

    @Override
    public Optional<SchoolGroup> findByName(String name) {
        return schoolGroupRepository.findByName(name);
    }

    @Override
    public List<SchoolGroup> findByYear(Date year) {
        return schoolGroupRepository.findByYear(year);
    }

    @Override
    public Optional<SchoolGroup> findById(Long id) {
        return schoolGroupRepository.findById(id);
    }

    @Override
    public List<SchoolGroup> findAll() {
        return schoolGroupRepository.findAll();
    }

    @Override
    public SchoolGroup save(SchoolGroup object) {

        Course course = new Course();
        if(object.getCourse().getId() == null){
            object.setCourse(courseRepository.save(course));
        }else{
            course=courseRepository.findById(object.getCourse().getId()).orElseThrow(() -> new ResourceNotFoundException("Course", "id", object.getCourse().getId()));
        }
        object.setCourse(new Course());
        object.addCourse(course);

        Factory factory= new Factory();

        if(object.getFactory().getId() == null){
            object.setFactory(factoryRepository.save(factory));
        }else{
            factory=factoryRepository.findById(object.getFactory().getId()).orElseThrow(()->new ResourceNotFoundException("Factory", "id", object.getFactory().getId()));
        }
        object.setFactory(new Factory());
        object.addFactory(factory);

        return schoolGroupRepository.save(object);
    }

    @Override
    public void delete(SchoolGroup object) {
        schoolGroupRepository.delete(object);
    }
}
