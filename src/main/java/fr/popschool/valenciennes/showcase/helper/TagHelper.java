package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.model.User;

import java.util.Set;

public interface TagHelper {
    void addProject(Project p);

    void addProjects(Set<Project> projects);

    void removeProject(Project p);

    void addUser(User user);

    void addUsers(Set<User> users);

    void removeUser(User user);
}
