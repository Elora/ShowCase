package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.Project;

import java.util.Set;

public interface FileHelper {
    void addProject(Project p);

    void removeProject(Project p);
}
