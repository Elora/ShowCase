package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.*;

import java.util.Set;

public interface ProjectHelper {

    void addUser(User user);

    void removeUser(User user);

    void addUsers(Set<User> users);

    void addLink(Link l);

    void addLinks(Set<Link> links);

    void removeLink(Link l);

    void addFile(File f);

    void addFiles(Set<File> files);

    void removeFile(File f);

    void addTag(Tag t);

    void addTags(Set<Tag> tags);

    void removeTag(Tag t);

    void addCategory(Category c);

    void removeCategory(Category c);

    void addCategories(Set<Category> categories);
}
