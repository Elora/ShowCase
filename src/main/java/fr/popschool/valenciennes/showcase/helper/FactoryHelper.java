package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.Course;
import fr.popschool.valenciennes.showcase.model.SchoolGroup;

import java.util.Set;

public interface FactoryHelper {
    void addSchoolGroup(SchoolGroup sg);

    void removeSchoolGroup(SchoolGroup sg);

    void addSchoolGroups(Set<SchoolGroup> schoolGroups);

    void addCourse(Course c);

    void addCourses(Set<Course> courses);

    void removeCourse(Course c);
}
