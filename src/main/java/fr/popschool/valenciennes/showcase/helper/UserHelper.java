package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.File;
import fr.popschool.valenciennes.showcase.model.Link;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.model.Tag;

import java.util.Set;

public interface UserHelper {

    void addProject(Project p);

    void addProjects(Set<Project> projects);

    void removeProject(Project p);

    void addTag(Tag t);

    void addTags(Set<Tag> tags);

    void removeTag(Tag t);

}
