package fr.popschool.valenciennes.showcase.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.popschool.valenciennes.showcase.helper.ProjectHelper;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="projects")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Project implements ProjectHelper {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    private String name;
    @NotBlank
    private String creator;
    private Date startDate;
    private Date endDate;
    private String description;



    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name="projects_categories",
            joinColumns = {@JoinColumn(name ="project_id")},
            inverseJoinColumns = {@JoinColumn(name="category_id")}
    )
    private Set<Category> categories = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name="projects_tags",
            joinColumns = {@JoinColumn(name ="project_id")},
            inverseJoinColumns = {@JoinColumn(name="tag_id")}
    )
    private Set<Tag> tags= new HashSet<>();


    @ManyToMany(mappedBy = "projects")
    private Set<User> users= new HashSet<>();

    @OneToMany(mappedBy = "projects")
    private Set<File> files= new HashSet<>();

    @OneToMany(mappedBy = "projects")
    private Set<Link> links= new HashSet<>();

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
    }

    public Set<Link> getLinks() {
        return links;
    }

    public void setLinks(Set<Link> links) {
        this.links = links;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }


    public Project(String name, String creator, Date startDate, Date endDate){
        this.startDate=startDate;
        this.endDate=endDate;
        this.name=name;
        this.creator=creator;
    }

    public Project() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }



    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public void addUser(User user) {
        this.users.add(user);
        user.getProjects().add(this);
    }

    @Override
    public void removeUser(User user) {
        this.users.remove(user);
        user.getProjects().remove(user);
    }

    @Override
    public void addUsers(Set<User> users) {
        this.users.addAll(users);
        for (User us :
                users) {
            us.getProjects().add(this);
        }
    }

    @Override
    public void addLink(Link link) {
        this.links.add(link);
        link.setProjects(this);
    }

    @Override
    public void addLinks(Set<Link> links) {
        this.links.addAll(links);
        for (Link l: links){
            l.setProjects(this);
        }
    }

    @Override
    public void removeLink(Link link) {
        this.links.remove(link);
        link.setProjects(null);
    }

    @Override
    public void addFile(File file) {
        this.files.add(file);
        file.setProjects(this);
    }

    @Override
    public void addFiles(Set<File> files) {
        this.files.addAll(files);
        for(File f: files){
            f.setProjects(this);
        }
    }

    @Override
    public void removeFile(File file) {
        this.files.remove(file);
        file.setProjects(null);
    }

    @Override
    public void addTag(Tag t) {
        this.tags.add(t);
        t.getProjects().add(this);
    }

    @Override
    public void addTags(Set<Tag> tags) {
        this.tags.addAll(tags);
        for (Tag tag : tags){
            tag.getProjects().add(this);
        }
    }

    @Override
    public void removeTag(Tag t) {
        this.tags.remove(t);
        t.getProjects().remove(this);
    }

    @Override
    public void addCategory(Category c) {
        this.categories.add(c);
        c.getProjects().add(this);
    }


    @Override
    public void removeCategory(Category c) {
        this.categories.remove(c);
        c.getProjects().remove(this);
    }

    @Override
    public void addCategories(Set<Category> categories) {
        this.categories.addAll(categories);
        for (Category category : categories){
            category.getProjects().add(this);
        }
    }
}
