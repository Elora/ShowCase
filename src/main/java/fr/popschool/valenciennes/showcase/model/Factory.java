package fr.popschool.valenciennes.showcase.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.popschool.valenciennes.showcase.helper.FactoryHelper;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="factories")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Factory implements FactoryHelper{

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    private String name;
    private String city;

    @OneToMany(mappedBy = "factory")
    private Set<SchoolGroup> schoolGroup= new HashSet<>();

    @ManyToMany(mappedBy = "factories")
    private Set<Course> courses= new HashSet<>();

    public Factory(String name) {
        this.name = name;
    }

    public Factory(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public Set<SchoolGroup> getSchoolGroup() {
        return schoolGroup;
    }

    public void setSchoolGroup(Set<SchoolGroup> schoolGroup) {
        this.schoolGroup = schoolGroup;
    }

    @Override
    public void addSchoolGroup(SchoolGroup sg) {
        this.getSchoolGroup().add(sg);
        sg.setFactory(this);
    }

    @Override
    public void removeSchoolGroup(SchoolGroup sg) {
        this.getSchoolGroup().remove(sg);
        sg.setFactory(null);
    }

    @Override
    public void addCourse(Course c) {
        this.courses.add(c);
        c.getFactories().add(this);
    }

    @Override
    public void removeCourse(Course c) {
        this.courses.remove(c);
        c.getFactories().remove(this);
    }

    @Override
    public void addSchoolGroups(Set<SchoolGroup> schoolGroups) {
        this.schoolGroup.addAll(schoolGroups);
        for(SchoolGroup sg: schoolGroups){
            sg.setFactory(this);
        }
    }

    @Override
    public void addCourses(Set<Course> courses) {
        this.courses.addAll(courses);
        for(Course c: courses){
            c.getFactories().add(this);
        }
    }
}
