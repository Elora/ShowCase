package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.exception.BadRequestException;
import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Category;
import fr.popschool.valenciennes.showcase.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class CategoryController implements DefaultController<Category>{
    @Autowired
    private CategoryService categoryService;

    @Override
    @PostMapping(value = "/project_category", produces = MediaType.APPLICATION_JSON_VALUE)
    public Category create(@RequestBody Category object) {
        return categoryService.save(object);
    }

    @Override
    @GetMapping(value = "/project_category/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Category findById(@PathVariable("id") Long id) {
        return categoryService.findById(id).orElseThrow(()->new ResourceNotFoundException("Category","Id",id));
    }

    @Override
    @GetMapping(value = "/project_categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> findAll() {
        return categoryService.findAll();
    }

    @Override
    @PutMapping(value = "/project_category/{id}")
    public Category update(@PathVariable("id") Long id, @RequestBody Category object) {
        categoryService.findById(id).orElseThrow(()->new ResourceNotFoundException("Category","Id",id));
        return categoryService.save(object);
    }

    @Override
    @DeleteMapping(value = "/projet_category")
    public void delete(@RequestBody Category object) {
        categoryService.delete(object);
    }

    @GetMapping (value = "/project_categories/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<Category> findByName(@RequestParam(value = "name", required = false) String name){
        if (name!=null) {
            return categoryService.findByName(name);
        }
        throw new BadRequestException("Param name is required");
    }
}
