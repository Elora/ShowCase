package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.exception.BadRequestException;
import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Factory;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.service.FactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class FactoryController implements DefaultController<Factory> {

    @Autowired
    FactoryService factoryService;

    @Override
    @PostMapping(value = "/factory", produces = MediaType.APPLICATION_JSON_VALUE)
    public Factory create(@RequestBody Factory object) {
        return factoryService.save(object);
    }

    @Override
    @GetMapping(value = "/factory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Factory findById(@PathVariable("id") Long id) {
        return factoryService.findById(id).orElseThrow(()->new ResourceNotFoundException("Factory", "Id", id));
    }

    @Override
    @GetMapping(value = "/factories", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Factory> findAll() {
        return factoryService.findAll();
    }

    @Override
    @PutMapping(value = "/factory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Factory update(@PathVariable("id") Long id, @RequestBody Factory object) {
        Factory factory= factoryService.findById(id).orElseThrow(()-> new ResourceNotFoundException("Factory", "Id", id));
        return factoryService.save(object);
    }

    @Override
    @DeleteMapping(value = "/factory")
    public void delete(Factory object) {
        factoryService.delete(object);
    }

   /* @GetMapping(value = "/factories/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Factory> findByNameOrCity(@RequestParam(value = "name", required = false) String name, @RequestParam(value="city", required= false) String city){
        if (name!=null){
            return factoryService.findByName(name);
        }
        if (city!= null){
            return factoryService.findByCity(city);
        }
        throw new BadRequestException("Param name or creator is required");
    }*/

}
