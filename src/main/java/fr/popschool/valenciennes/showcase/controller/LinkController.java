package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.model.Link;
import fr.popschool.valenciennes.showcase.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api")
public class LinkController implements DefaultController<Link> {
    @Autowired
    LinkService linkService;

    @Override
    @PostMapping(value = "/link", produces = MediaType.APPLICATION_JSON_VALUE)
    public Link create(@RequestBody Link object) {
        return linkService.save(object);
    }

    @Override
    @GetMapping(value = "/link/{id}")
    public Link findById(Long id) {
        return null;
    }

    @Override
    public List<Link> findAll() {
        return null;
    }

    @Override
    public Link update(Long id, Link object) {
        return null;
    }

    @Override
    public void delete(Link object) {

    }
}
