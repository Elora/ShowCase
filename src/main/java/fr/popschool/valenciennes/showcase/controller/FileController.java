package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.File;
import fr.popschool.valenciennes.showcase.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FileController implements DefaultController<File> {

    @Autowired
    FileService fileService;

    @Override
    @PostMapping(value = "/file", produces = MediaType.APPLICATION_JSON_VALUE)
    public File create(@RequestBody File object) {
        return fileService.save(object);
    }

    @Override
    @GetMapping(value = "/file/{id}")
    public File findById(@PathVariable("id") Long id) {
        return fileService.findById(id).orElseThrow(()-> new ResourceNotFoundException("File", "Id", id));
    }

    @Override
    @GetMapping(value = "/files", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<File> findAll() {
        return fileService.findAll();
    }

    @Override
    @PutMapping(value = "/file/{id}")
    public File update(@PathVariable("id") Long id, @RequestBody File object) {
        fileService.findById(id).orElseThrow(()->new ResourceNotFoundException("File","Id", id));
        return fileService.save(object);
    }

    @Override
    @DeleteMapping(value = "/file")
    public void delete(File object) {
        fileService.delete(object);
    }
}
