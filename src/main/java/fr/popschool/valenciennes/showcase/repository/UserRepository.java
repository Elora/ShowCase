package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> findByEmail(String email);
    Boolean existsByLogin(String login);

    Boolean existsByEmail(String email);

}
